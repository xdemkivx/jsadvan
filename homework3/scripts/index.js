/* 
Деструкторизация это изменения масива или обьэкта, для того что б взять
с него только нужние елементы, например в обьекте больше 20 елементов а
нам надо два, мы с помощью Деструкторизация можем это сделать, 
или обьект перевести в масив, и использовать свойства масива по ключ
станет индексом, или для обьяденения обьэктов или масивов, или и того и 
другого можно даже в определенном порядке. но нижняя информация дублирующа 
всегда будет переписывать верхнюю, просто классная вещь мастхев, без нее
никуда.
*/
//1========================================
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
const clienALLCompani = [...clients1,...clients2];
console.log(clienALLCompani);

//2========================================================================
const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];

  const newArr=({name,lastName, age})=> ({name,lastName,age});
  const charactersShortInfo  = characters.map(newArr)

console.log(charactersShortInfo );
//3=============================================================
const user1 = {
    name: "John",
    years: 30
  };
  
  let {name,years : age,isadmin} = user1;
  console.log(name);
  console.log(age);
  console.log(isadmin);
  
  //4==================================================================
  const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
  }
  const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
  }
  const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
  }
  const satoshiall = {...satoshi2018, ...satoshi2019,...satoshi2020};
  console.log(satoshiall);
  //5============================================================
  
  const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
  const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
  }

  const newBooks= [...books, {...bookToAdd}];
  console.log(newBooks);


  //6==============================================================
  const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
  }
  const {name: n, surname, age:a = 50 , salary = 200000 } = employee;
  console.log(n);
  console.log(surname);
  console.log(a);
  console.log(salary);


  //7--------------------------------------------------------------------
  const array = ['value', () => 'showValue'];
  let [value,showValue] = array;

// Допишите ваш код здесь

alert(value); // должно быть выведено 'value'
alert(showValue());  // должно быть выведено 'showValue'