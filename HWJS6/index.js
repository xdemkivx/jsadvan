/* ## Теоретический вопрос
Обьясните своими словами, как вы понимаете асинхронность в Javascript

## Задание
Написать программу "Я тебя по айпи вычислю"

#### Технические требования:
- Создать простую HTML страницу с кнопкой `Вычислить по IP`.
- По нажатию на кнопку - отправить AJAX запрос по адресу `https://api.ipify.org/?format=json`, получить оттуда IP адрес клиента.
- Узнав IP адрес, отправить запрос на сервис `https://ip-api.com/` и получить информацию о физическом адресе.
- Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, страна, регион, город, район города.
- Все запросы на сервер необходимо выполнить с помощью async await.

## Примечание
Задание должно быть выполнено на "чистом" Javascript без использования библиотек типа jQuery или React.

#### Литература:
- [Async/await](https://learn.javascript.ru/async-await)
- [async function](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/async_function)
- [Документация сервиса ip-api.com](http://ip-api.com/docs/api:json)
 */


/* 1. осинхроность в джс происходит с помощью евент лупа, получаеться код попадает в стак, 
потом он выполняеться, и со стака удаляеться, если функция отложенная сет таим аут или асинк ефеит
она попадает в стак, потом попадает в очередь и код дальше парсится по стаку, а потом с очереди
функции переносяться в стак грде они выполняються. 
Код всехда получаеться идет однопоточный, просто асинхронная функция выводится с потока, ждет
в очереди свои данные или завсимую функцию, и потом возвращаеться в поток.
 */




const getIP = async () => {
  try {

      const ip = await fetch('https://api.ipify.org/?format=json').then(response => response.json());
      console.log(ip)
      return ip;
  }
  catch (err) {
      console.error(err);
  }
}


const showLocation = async () => {

  try {
      const userIP = await getIP();

      const url = `http://ip-api.com/json/${userIP.ip}`
      console.log(url)

      const location = await fetch(url).then(response => response.json());
      console.log(location)

      const { as, country, city, countryCode, isp, query, region, regionName, timezone, zip, lat, lon } = location
      const ul = document.querySelector('ul')
      if (ul) { ul.remove() }

      document.body.insertAdjacentHTML('beforeend', `<ul><li>as:${as}</li><li>City:${city}</li><li>Country:${country}</li><li>CountryCode:${countryCode}</li><li>isp:${isp}</li><li>lat:${lat}</li><li>lon:${lon}</li><li>Region:${region}</li><li>Query:${query}</li><li>RegionName:${regionName}</li><li>Timezone:${timezone}</li><li>Zip:${zip}</li></ul>`)


      return location
  }
  catch (err) {
      console.error(err)
  }

}

const findMeButton = document.querySelector('.findMe_btn')

findMeButton.addEventListener('click', showLocation)