const modalDeclarations =[
    {id:1,
    header:'Do you want to delete this file?',
    backgroundColor:'tomato',
    color:'#fff',
    text:'Once you delete this file,it wont be possible to undo this action.Are you sure you want to delete it'},
    {id:2,
        backgroundColor:'aquablue',
        color:'#1585d3',
    header:'save Modal Title',
    text:'save Modal Text'}
]

export default modalDeclarations;